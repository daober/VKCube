//#define GLFW_INCLUDE_VULKAN
#include <vulkan\vulkan.h>
//#include <GLFW\glfw3.h>

#include "../include/Shader.h"

#include <fstream>
#include <iostream>




Shader::Shader( const VkDevice & dev ) {
	this->logicalDevice = dev;
}

//does the same as the overloaded constructor (not used)
void Shader::init( const VkDevice & dev ) {
	this->logicalDevice = dev;
}

/*========================================================
readShaderFile
	reads the file (+path to file needed)
	as byte code and allocates a file stream buffer
	returns the loaded filestream as byte in a 
	std::vector<char> 
=========================================================*/
std::vector<char> Shader::readShaderFile( const std::string & filename ) {
	//read the file as binary and start reading at the end of the file (ate)

	//the advantage of starting to read at the end of the file is that we can use the read position
	//to determine the size of the file and allocate a buffer
	std::ifstream file( filename, std::ios::ate | std::ios::binary );	

	size_t fileSize = file.tellg();		//char (binary) size of file
	//allocate buffer size
	std::vector<char> buffer( fileSize );

	//now seek back to the beginning of the file and read all the bytes at once
	file.seekg( std::ios::beg );			//rewind
	file.read( buffer.data(), fileSize );

	file.close();

	if ( file.bad() ) {
		throw std::runtime_error( "error reading shader file, loss of stream integrity" );
	} else {
		//std::cout << "shaderfile: " << filename << " loaded successfully" << std::endl;
	}

	return buffer;
}



/*========================================================
createShaderModule
	before passing the code to the pipeline 
	it is mandatory to wrap it in a VkShaderModule object
=========================================================*/
VkShaderModule Shader::createShaderModule( const std::vector<char>& byteCode ) {
	
	VkShaderModuleCreateInfo createInfo = {};

	createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	createInfo.codeSize = byteCode.size();
	//bytecode is specified in bytes but bytcode pointer is a uint32_t pointer
	//rather than a char pointer, therefore we perform a reinterpret_cast to uint32_t*
	createInfo.pCode = reinterpret_cast<const uint32_t*>(byteCode.data());

	VkShaderModule shaderModule;
	
	if ( vkCreateShaderModule( this->logicalDevice, &createInfo, nullptr, &shaderModule ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to create shader module" );
	}

	return shaderModule;
}
