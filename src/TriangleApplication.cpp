#include "../include/TriangleApplication.h"
#include "../include/VertexData.h"

#include <iostream>
#include <set>
#include <algorithm>
#include <chrono>
#include <fstream>


#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>


/*==============================================================
CreateDebugReportCallbackEXT
	load the additional extension function
	NOTE: not part of this class
================================================================*/
VkResult CreateDebugReportCallbackEXT( VkInstance instance,
	const VkDebugReportCallbackCreateInfoEXT* pCreateInfo,
	const VkAllocationCallbacks* pAllocator,
	VkDebugReportCallbackEXT* pCallback ) {

	auto func = ( PFN_vkCreateDebugReportCallbackEXT ) vkGetInstanceProcAddr( instance, "vkCreateDebugReportCallbackEXT" );

	if ( func != nullptr ) {
		return func( instance, pCreateInfo, pAllocator, pCallback );
	} else {
		return VK_ERROR_EXTENSION_NOT_PRESENT;
	}
}



/*==============================================================
DestroyDebugReportCallbackEXT
	delete the additional extension function
	NOTE: not part of this class
================================================================*/
void DestroyDebugReportCallbackEXT( VkInstance instance, VkDebugReportCallbackEXT callback, const VkAllocationCallbacks* pAllocator ) {
	auto func = ( PFN_vkDestroyDebugReportCallbackEXT ) vkGetInstanceProcAddr( instance, "vkDestroyDebugReportCallbackEXT" );
	if ( func != nullptr ) {
		func( instance, callback, pAllocator );
	}
}



void TriangleApplication::run() {
	initWindow();					//initialize GLFW window manager
	initVulkan();					//initialize Vulkan
	loop();							//main render loop
	cleanup();						//cleanup method
}



void TriangleApplication::initVulkan() {
	createInstance();				//initialize vulkan instance
	setupDebugCallback();			//intialize validation layer callback
	createSurface();				//NOTE: needs to be called before picking the physical device
	pickPhysicalDevice();			//selects a physical device
	createLogicalDevice();			//create a logical device
	createVMAllocator( );
	createSwapChain();				//create the swap chain
	createImageViews();				//create the image views
	createRenderPass();				//create the render pass
	createDescriptorLayout( );
	createGraphicsPipeline();		//create the graphics pipeline
	createCommandPool( );			//create the command pool
	createDepthBuffer( );
	createFramebuffers();			//create the framebuffer object
	createTextureImage( );
	createTextureImageView( );
	createTextureSampler( );
	createVertexBuffer();
	createIndexBuffer();
	createUniformBuffers( );
	createDescriptorPool( );
	createDescriptorSets( );
	createCommandBuffers();			//create the command buffers
	createSyncObjects();			//create the semaphores
}



void TriangleApplication::initWindow() {
	//initialize glfw at first
	glfwInit();
	//tell that we don't want to create an OpenGL context
	glfwWindowHint( GLFW_CLIENT_API, GLFW_NO_API );
	//do not resize window
	glfwWindowHint( GLFW_RESIZABLE, GLFW_FALSE );
	//create glfw window
	window = glfwCreateWindow( SCR_WIDTH, SCR_HEIGHT, "VKTriangle", nullptr, nullptr );
	//register callback for closing the window on escape
	glfwSetKeyCallback( window, key_callback );

	glfwSetWindowUserPointer( window, this );
	glfwSetFramebufferSizeCallback( window, framebufferResizeCallback );
}



void TriangleApplication::loop() {
	while ( !glfwWindowShouldClose( window ) ) {
		glfwPollEvents();
		drawFrame();
	}
	//wait for idle state
	vkDeviceWaitIdle( logicalDevice );
}



void TriangleApplication::cleanupSwapChain() {

	if ( depthImageView != VK_NULL_HANDLE ) {
		vkDestroyImageView( logicalDevice, depthImageView, nullptr );
		depthImageView = VK_NULL_HANDLE;
	}

	if ( depthImage != VK_NULL_HANDLE ) {
		vmaDestroyImage( allocator, depthImage, depthAlloc );
		depthImage = VK_NULL_HANDLE;
	}

	for ( size_t i = 0; i != swapChainFramebuffers.size(); i++ ) {
		vkDestroyFramebuffer( logicalDevice, swapChainFramebuffers[i], nullptr );
	}

	//not destroying but freeing
	//this way we can reuse the existing pool to allocate new 
	//command buffers instead of recreating the command pool
	//from scratch
	vkFreeCommandBuffers( logicalDevice, commandPool, commandBuffers.size(), commandBuffers.data() );

	vkDestroyPipeline( logicalDevice, graphicsPipeline, nullptr );
	vkDestroyPipelineLayout( logicalDevice, pipelineLayout, nullptr );
	vkDestroyRenderPass( logicalDevice, renderPass, nullptr );
	vkDestroySampler( logicalDevice, textureSampler, nullptr );
	vkDestroyImageView( logicalDevice, textureImageView, nullptr );

	for ( size_t i = 0; i != swapChainImageViews.size(); i++ ) {
		vkDestroyImageView( logicalDevice, swapChainImageViews[i], nullptr );
	}

	vkDestroySwapchainKHR( logicalDevice, swapChain, nullptr );
}



void TriangleApplication::cleanup() {

	char* statsString = "stats";
	vmaBuildStatsString( allocator, &statsString, true );
	//printf( "%s\n", statsString );

	std::ofstream ostream;
	ostream.open( "dumpVis.json" );
	ostream << statsString;
	ostream.close( );

	vmaFreeStatsString( allocator, statsString );

	cleanupSwapChain();

	vkDestroyDescriptorPool( logicalDevice, descriptorPool, nullptr );
	vkDestroyDescriptorSetLayout( logicalDevice, descriptorSetLayout, nullptr );


	if ( textureImage != VK_NULL_HANDLE ) {
		vmaDestroyImage( allocator, textureImage, texAlloc );
		textureImage = VK_NULL_HANDLE;
	}

	if ( indexBuffer != VK_NULL_HANDLE ) {
		vmaDestroyBuffer( allocator, indexBuffer, indexAlloc );
		indexBuffer = VK_NULL_HANDLE;
	}

	if ( vertexBuffer != VK_NULL_HANDLE ) {
		vmaDestroyBuffer( allocator, vertexBuffer, vertexAlloc );
		vertexBuffer = VK_NULL_HANDLE;
	}

	for ( size_t i = 0; i < swapChainImages.size( ); i++ ) {
		if ( uniformBuffer.at(i) != VK_NULL_HANDLE ) {
			vmaDestroyBuffer( allocator, uniformBuffer.at(i), uniformAlloc.at(i) );
			uniformBuffer.at(i) = VK_NULL_HANDLE;
		}
	}

	//destroys the synchronization objects (semaphores and fences) one by one
	for ( size_t i = 0; i != MAX_FRAMES_IN_FLIGHT; i++ ) {
		vkDestroySemaphore( logicalDevice, renderFinishedSemaphore[i], nullptr );
		vkDestroySemaphore( logicalDevice, imageAvailableSemaphore[i], nullptr );
		vkDestroyFence( logicalDevice, inFlightFences[i], nullptr );
	}

	vkDestroyCommandPool( logicalDevice, commandPool, nullptr );

	if ( allocator != VK_NULL_HANDLE ) {
		vmaDestroyAllocator( allocator );
		allocator = nullptr;
	}


	if ( enableValidationLayers ) {
		DestroyDebugReportCallbackEXT( instance, validationCallback, nullptr );
	}

	vkDestroyDevice( logicalDevice, nullptr );
	vkDestroySurfaceKHR( instance, surface, nullptr );
	vkDestroyInstance( instance, nullptr );

	glfwDestroyWindow( window );

	glfwTerminate();
}



/*======================================================================
checkValidationLayerSupport
	checks if all of the requested layers are available
========================================================================*/
bool TriangleApplication::checkValidationLayerSupport() {
	uint32_t layerCount;

	vkEnumerateInstanceLayerProperties( &layerCount, nullptr );

	std::vector<VkLayerProperties> availableLayers( layerCount );
	vkEnumerateInstanceLayerProperties( &layerCount, availableLayers.data() );

	//check if all the layers in validationLayers exist in the availableLayers list
	for ( const char* layerName : validationLayers ) {
		bool layerFound = false;

		for ( const VkLayerProperties& layerProp : availableLayers ) {
			if ( strcmp( layerName, layerProp.layerName ) == 0 ) {
				layerFound = true;
				break;
			}
		}
		if ( !layerFound ) {
			return false;
		}
	}

	return true;
}



void TriangleApplication::setupDebugCallback() {
	if ( !enableValidationLayers ) {
		return;
	}

	VkDebugReportCallbackCreateInfoEXT createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
	createInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
	//finally set the callback
	createInfo.pfnCallback = debugCallback;

	if ( CreateDebugReportCallbackEXT( instance, &createInfo, nullptr, &validationCallback ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to set up debug callback" );
	}

}



/*======================================================================
pickPhysicalDevice
	picks and sets an suitable physical Vulkan device
========================================================================*/
void TriangleApplication::pickPhysicalDevice() {
	
	//check if there are devices with vulkan support available
	uint32_t deviceCount = 0;
	vkEnumeratePhysicalDevices( instance, &deviceCount, nullptr );

	if ( !deviceCount ) {
		throw std::runtime_error( "failed to find GPUs with Vulkan support!" );
	}

	//now allocate an array to hold all the VkPhysicalDevice handles
	std::vector<VkPhysicalDevice> devices( deviceCount );
	vkEnumeratePhysicalDevices( instance, &deviceCount, devices.data() );

	//go through the array and check each PhysicalDevice for vulkan support
	for ( const VkPhysicalDevice& dev : devices ) {
		if ( isDeviceSuitable( dev ) ) {
			this->physicalDevice = dev;
			break;
		}
	}

	if ( this->physicalDevice == VK_NULL_HANDLE ) {
		throw std::runtime_error( "no suitable GPU for Vulkan available" );
	}

}



void TriangleApplication::createLogicalDevice() {

	QueueFamilyIndices indices = findQueueFamilies( physicalDevice );

	std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
	std::set<int> uniqueQueueFamilies = { indices.graphicsFamily, indices.presentFamily };

	float queuePriority = 1.0f;

	//assign the queues which should be activated on the logical device
	for ( int queueFamily : uniqueQueueFamilies ) {
		VkDeviceQueueCreateInfo queueCreateInfo = {};
		queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueFamilyIndex = queueFamily;
		queueCreateInfo.queueCount = 1;
		queueCreateInfo.pQueuePriorities = &queuePriority;
		queueCreateInfos.push_back( queueCreateInfo );
	}
	
	//device features
	VkPhysicalDeviceFeatures deviceFeatures = {};
	deviceFeatures.samplerAnisotropy = VK_TRUE;

	//now create the logical device
	VkDeviceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	
	createInfo.queueCreateInfoCount = queueCreateInfos.size();
	createInfo.pQueueCreateInfos = queueCreateInfos.data();

	createInfo.pEnabledFeatures = &deviceFeatures;

	createInfo.enabledExtensionCount = deviceExtensions.size();
	createInfo.ppEnabledExtensionNames = deviceExtensions.data();

	//enable the same validation layers as done with vkInstance
	//we dont need any device specific extensions for now
	if ( enableValidationLayers ) {
		createInfo.enabledLayerCount = validationLayers.size();
		createInfo.ppEnabledLayerNames = validationLayers.data();
	} else {
		createInfo.enabledLayerCount = 0;
	}

	//instanciate the logical device
	if ( vkCreateDevice( physicalDevice, &createInfo, nullptr, &logicalDevice ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to create logical device" );
	}

	//the queues are automatically created along with the device, but we dont
	//have a handle to interface with them yet

	//retrieve queue handles for each queue family
	//graphicsFamily handle
	vkGetDeviceQueue( logicalDevice, indices.graphicsFamily, 0, &graphicsQueue );
	//presentFamily handle
	vkGetDeviceQueue( logicalDevice, indices.presentFamily, 0, &presentQueue );

	//std::cout << "indices of graphicsFamily: " << (indices.graphicsFamily) << std::endl;
	//std::cout << "indices of logicalDevice: " << (indices.presentFamily) << std::endl;

}



/*======================================================================
createSwapChain
	constructs the swap chain
========================================================================*/
void TriangleApplication::createSwapChain() {

	//query all swap chain supports
	SwapChainSupportDetails swapChainSupport = querySwapChainSupport( physicalDevice );
	
	VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat( swapChainSupport.formats );
	VkPresentModeKHR presentMode = chooseSwapPresentMode( swapChainSupport.presentModes );
	VkExtent2D extent = chooseSwapExtent( swapChainSupport.capabilites );

	//queue length
	//specifies the minimum amount of images to function properly
	uint32_t imageCount = swapChainSupport.capabilites.minImageCount + 1;
	if ( swapChainSupport.capabilites.maxImageCount > 0 && imageCount > swapChainSupport.capabilites.maxImageCount ) {
		imageCount = swapChainSupport.capabilites.maxImageCount;
	}

	//now create a createinfo as usual and fill in the appropriate data
	VkSwapchainCreateInfoKHR createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	createInfo.surface = surface;
	
	createInfo.minImageCount = imageCount;
	createInfo.imageFormat = surfaceFormat.format;
	createInfo.imageColorSpace = surfaceFormat.colorSpace;
	createInfo.imageExtent = extent;
	createInfo.imageArrayLayers = 1;			//always 1 unless for stereoscopic 3d applications
	createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;		//we want to render into them (use them as render targets)

	//specify how to handle swap chain images that will be used across multiple queue families
	//we draw the images in the swap chain from the graphics queue and submitting them on the presentation queue
	//in most graphics cards the graphics queue and present queue family are the same
	//so we can use VK_SHARING_MODE_EXCLUSIVE without restorting to CONCURRENT MODE which
	//the solving of concurrency problems
	QueueFamilyIndices indices = findQueueFamilies( physicalDevice );

	uint32_t queueFamilyIndices[] = { ( uint32_t ) indices.graphicsFamily, ( uint32_t ) indices.presentFamily };

	//if not the same indices of graphicsfamily and presentfamily
	if ( indices.graphicsFamily != indices.presentFamily ) {
		createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		//queueFamilyIndexCount is the number of queue families having access to the images of the swapchain in case imageSharingMode is VK_SHARING_MODE_CONCURRENT.
		createInfo.queueFamilyIndexCount = 2;
		//QueueFamilyIndices is an array of queue family indices having access to the images of the swapchain in case imageSharingMode is VK_SHARING_MODE_CONCURRENT.
		createInfo.pQueueFamilyIndices = queueFamilyIndices;		
	} else {
		createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;	//that is what we want
		createInfo.queueFamilyIndexCount = 0;						//optional
		createInfo.pQueueFamilyIndices = nullptr;
	}

	createInfo.preTransform = swapChainSupport.capabilites.currentTransform;
	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

	createInfo.presentMode = presentMode;
	createInfo.clipped = VK_TRUE;

	/*With Vulkan it's possible that your swap chain becomes invalid or unoptimized
	while your application is running, for example because the window was resized. 
	In that case the swap chain actually needs to be recreated from scratch and 
	a reference to the old one must be specified in this field*/
	createInfo.oldSwapchain = VK_NULL_HANDLE;
	
	//now finally create the swapchain
	if ( vkCreateSwapchainKHR( logicalDevice, &createInfo, nullptr, &swapChain ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to create swap chain" );
	}

	//retrieve the swapChainImage handle
	vkGetSwapchainImagesKHR( logicalDevice, swapChain, &imageCount, nullptr );
	swapChainImages.resize( imageCount );
	vkGetSwapchainImagesKHR( logicalDevice, swapChain, &imageCount, swapChainImages.data() );

	//store the image format and swap chain extent in members too
	swapChainImageFormat = surfaceFormat.format;
	swapChainExtent = extent;
}



void TriangleApplication::createVMAllocator( ) {
	VmaAllocatorCreateInfo allocatorInfo = {};
	allocatorInfo.physicalDevice = physicalDevice;
	allocatorInfo.device = logicalDevice;

	if ( vmaCreateAllocator( &allocatorInfo, &allocator ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to create vma allocator" );
	}
}



/*======================================================================
isDeviceSuitable
	helper function to for base device suitability checks
========================================================================*/
bool TriangleApplication::isDeviceSuitable( VkPhysicalDevice device ) {

	QueueFamilyIndices indices = findQueueFamilies( device );

	bool extensionsSupported = checkDeviceExtensionSupport( device );

	bool swapChainAdequate = false;

	if ( extensionsSupported ) {
		SwapChainSupportDetails swapChainSupport = querySwapChainSupport( device );
		swapChainAdequate = !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty();
	}

	return indices.isComplete() && extensionsSupported && swapChainAdequate;
}



bool TriangleApplication::checkDeviceExtensionSupport( VkPhysicalDevice device ) {

	uint32_t extensionsCount;
	vkEnumerateDeviceExtensionProperties( device, nullptr, &extensionsCount, nullptr );

	std::vector<VkExtensionProperties> availableExtensions( extensionsCount );
	vkEnumerateDeviceExtensionProperties( device, nullptr, &extensionsCount, availableExtensions.data() );

	std::set<std::string> requiredExtensions( deviceExtensions.begin(), deviceExtensions.end() );

	for ( const VkExtensionProperties& ext : availableExtensions ) {
		//if available extensions are already succefully loaded -> then erase it from required extensions
		//since already avail
		requiredExtensions.erase( ext.extensionName );
	}

	return requiredExtensions.empty();
}



/*======================================================================
checkInstanceExtensions
	optional method: checks which extensions are supported
	also checks glfwExtensions if provided in parameter
========================================================================*/
void TriangleApplication::checkInstanceExtensions( const char ** glfwExtensions, const uint32_t glfwExtCount ) {

	//first check which extensions are supported before creating the instance
	uint32_t extensionCount = 0;
	vkEnumerateInstanceExtensionProperties( nullptr, &extensionCount, nullptr );

	std::vector<VkExtensionProperties> extensions( extensionCount );
	//query extension details
vkEnumerateInstanceExtensionProperties( nullptr, &extensionCount, extensions.data() );

std::cout << "available extensions from vulkan device: " << std::endl;

for ( const VkExtensionProperties& ext : extensions ) {
	std::cout << "SpecificationVersion \t" << ext.specVersion << "\t" << " ExtensionName: \t" << ext.extensionName << std::endl;
}

std::cout << std::endl;


//check if glfw vulkan extensions are also available
if ( glfwExtensions != nullptr ) {
	std::cout << "available extensions from glfw: " << std::endl;

	for ( int i = 0; i != 2; ++i ) {
		if ( glfwExtensions[i] != nullptr ) {
			const char* ext = glfwExtensions[i];
			std::cout << ext << std::endl;
			if ( strcmp( ext, "VK_KHR_surface" ) ^ strcmp( ext, "VK_KHR_win32_surface" ) ) {
				std::cout << "GLFW has appropriate Vulkan extensions" << std::endl;
			}
		} else {
			continue;
		}
	}
}

}



/*======================================================================
createRenderPass
	we need to tell Vulkan about the framebuffer attachments that will be
	used while rendering. we need to specify how many color and depth buffers
	there will be, how many samples to use for each of them and how their
	contents should be handled througout the rendering operations
========================================================================*/
void TriangleApplication::createRenderPass() {

	VkAttachmentDescription colorAttachment = {};
	colorAttachment.format = swapChainImageFormat;
	colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;	//not multisampling

	//determine what to do with the data in the attachment before and after rendering
	colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;				//clear the values at the start
	colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;				//rendered contents will be stored in memory and can be read later

	colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;	//irrelevant
	colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;	//irrelevant

	colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;			//specifies which layout the image will have before the render pass begins
	colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;		//images to be presented in the swap chain

	VkAttachmentDescription depthAttachment = {};
	depthAttachment.format = findDepthFormat( );
	depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
	depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	//a single render pass can consists of multiple subpasses
	VkAttachmentReference colorAttachmentRef = {};
	colorAttachmentRef.attachment = 0;									//specifies by its index the attachment descrition array
	colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;		//best performance

	VkAttachmentReference depthAttachmentRef = {};
	depthAttachmentRef.attachment = 1;
	depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	//now create the needed subpass
	VkSubpassDescription subpass = {};
	subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

	subpass.colorAttachmentCount = 1;
	subpass.pColorAttachments = &colorAttachmentRef;
	subpass.pDepthStencilAttachment = &depthAttachmentRef;

	//ensure that the render passes don't begin until the image is available
	VkSubpassDependency dependency = {};
	dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
	dependency.dstSubpass = 0;
	//specify the operations to wait on
	dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.srcAccessMask = 0;
	//wait for this stage
	dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	//the wait stage involves reading and writing of the color attachment
	dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

	std::array<VkAttachmentDescription, 2> attachments = { colorAttachment, depthAttachment };

	//render pass
	VkRenderPassCreateInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassInfo.attachmentCount = attachments.size( );
	renderPassInfo.pAttachments = attachments.data( );
	renderPassInfo.subpassCount = 1;
	renderPassInfo.pSubpasses = &subpass;
	//add the created dependency to color attachment stage
	renderPassInfo.dependencyCount = 1;
	renderPassInfo.pDependencies = &dependency;

	if ( vkCreateRenderPass( logicalDevice, &renderPassInfo, nullptr, &renderPass ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to create render pass" );
	}

}



void TriangleApplication::createSyncObjects() {

	imageAvailableSemaphore.resize( MAX_FRAMES_IN_FLIGHT );
	renderFinishedSemaphore.resize( MAX_FRAMES_IN_FLIGHT );
	inFlightFences.resize( MAX_FRAMES_IN_FLIGHT );

	VkSemaphoreCreateInfo semamphoreInfo = {};
	semamphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	VkFenceCreateInfo fenceInfo = {};
	fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	//we need to set a init flag
	fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	for ( size_t i = 0; i != MAX_FRAMES_IN_FLIGHT; i++ ) {
		//create the semaphores in the usual vulkan pattern
		if ( (vkCreateSemaphore( logicalDevice, &semamphoreInfo, nullptr, &imageAvailableSemaphore[i] ) != VK_SUCCESS)
			|| (vkCreateSemaphore( logicalDevice, &semamphoreInfo, nullptr, &renderFinishedSemaphore[i] ) != VK_SUCCESS)
			|| (vkCreateFence(logicalDevice, &fenceInfo, nullptr, &inFlightFences[i]) != VK_SUCCESS) ) {
			std::runtime_error( "failed to create synchronization objects for a frame" );
		}

	}

}



/*======================================================================
recreateSwapChain
	its possible for the window surface to change such that the swap
	chain is no clonge comptabile with it. one of the reasons that
	could cause this to happen is the size of the window changing
========================================================================*/
void TriangleApplication::recreateSwapChain() {

	int width = 0, height = 0;
	while ( width == 0 || height == 0 ) {
		glfwGetFramebufferSize( window, &width, &height );
		glfwWaitEvents();
	}

	//we shouldn't touch resources that may still be in use
	vkDeviceWaitIdle( logicalDevice );

	cleanupSwapChain();

	createSwapChain();
	createImageViews();
	createRenderPass();
	createGraphicsPipeline();
	createDepthBuffer( );
	createFramebuffers();
	createCommandBuffers();
}



/*======================================================================
findMemoryType
	graphics cards can offer different types of memory to allocate from.
	we need to combine the requirements of the buffer and our own
	application requirements to find the right type of memory to use
========================================================================*/
uint32_t TriangleApplication::findMemoryType( uint32_t typeFilter, VkMemoryPropertyFlags property ) {
	//query info about the available types of memory
	VkPhysicalDeviceMemoryProperties memProperties;
	vkGetPhysicalDeviceMemoryProperties( physicalDevice, &memProperties );

	for ( uint32_t i = 0; i < memProperties.memoryTypeCount; i++ ) {
		if ( (typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & property) == property ) {
			return i;
		}
	}

	throw std::runtime_error( "failed to find suitable memory type" );
}



void TriangleApplication::drawFrame() {
	//NOTE: fences can be accessed from your program using calls like
	//vkWaitForFences and semaphores cannot be.
	//Fences are mainly designed to synchronize your application itself with rendering operation
	//whereas semaphore are used to synchronize operations within or across command queues
	
	vkWaitForFences( logicalDevice, 1, &inFlightFences[currentFrame], VK_TRUE, std::numeric_limits<uint64_t>::max() );

	//acquiring an image from the swap chain
	uint32_t imageIndex;

	//swapchain from which we wish to aquire an image
	//timeout in nanoseconds for an image to become avail
	//using the max value of a 64 bit unsigned int disables the timeout
	//use of semaphore
	//additionally we need to find out if the swap chain recreation is necessarry
	VkResult result = vkAcquireNextImageKHR( logicalDevice, swapChain, std::numeric_limits<uint64_t>::max(), imageAvailableSemaphore[currentFrame], VK_NULL_HANDLE, &imageIndex );

	if ( result == VK_ERROR_OUT_OF_DATE_KHR ) {
		recreateSwapChain();
		return;
	} else if ( result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR ) {
		throw std::runtime_error( "failed to aquire swap chain image" );
	}

	updateUniformBuffer( imageIndex );

	//submitting the command buffer
	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

	VkSemaphore waitSemaphores[] = { imageAvailableSemaphore[currentFrame] };
	//we want to wait with writing colors to the image until it's (the image) available
	VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };

	submitInfo.waitSemaphoreCount = 1;
	submitInfo.pWaitSemaphores = waitSemaphores;
	submitInfo.pWaitDstStageMask = waitStages;

	submitInfo.commandBufferCount = 1;
	//which command buffers to actually submit for execution
	submitInfo.pCommandBuffers = &commandBuffers[imageIndex];

	//the signalSemaphore parameters specify which semaphores to signal 
	//once the command buffers have finished execution
	VkSemaphore signalSemaphores[] = { renderFinishedSemaphore[currentFrame] };
	submitInfo.signalSemaphoreCount = 1;
	submitInfo.pSignalSemaphores = signalSemaphores;

	//unlike semaphores, we need to restore the fences to unsignaled state by resetting it manually
	vkResetFences( logicalDevice, 1, &inFlightFences[currentFrame] );

	//we can now submit the command buffer to the graphics queue using vkQueueSubmit
	if ( vkQueueSubmit( graphicsQueue, 1, &submitInfo, inFlightFences[currentFrame] ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to submit draw command buffer" );
	}

	//presentation
	VkPresentInfoKHR presentInfo = {};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

	//specify which semaphores to wait on before presentation can happen
	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = signalSemaphores;

	//specify the swap chains to present images to
	VkSwapchainKHR swapChains[] = { swapChain };
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = swapChains;
	presentInfo.pImageIndices = &imageIndex;
	//allow to specify an array of VkResult values to check every 
	//individual swap chain if presentation was successful
	presentInfo.pResults = nullptr;			//optional

	//submit the request to present an image to the swap chain
	result = vkQueuePresentKHR( presentQueue, &presentInfo );

	if ( result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || framebufferResized ) {
		framebufferResized = false;
		recreateSwapChain();
	} else if ( result != VK_SUCCESS ) {
		throw std::runtime_error( "failed to present swap chain image" );
	}

	currentFrame = (currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
}



void TriangleApplication::createSurface() {
	//with glfw we can create a platform agnostic surface
	if ( glfwCreateWindowSurface( instance, window, nullptr, &surface ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to create window surface!" );
	}
}



void TriangleApplication::createImageViews() {
	//resize according to previously aquired swap chain image amount
	swapChainImageViews.resize( swapChainImages.size() );

	//iterate over all of the swap chain images
	for ( size_t i = 0; i != swapChainImages.size(); i++ ) {
		VkImageViewCreateInfo createInfo = {};

		createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		createInfo.image = swapChainImages.at( i );
		createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		createInfo.format = swapChainImageFormat;

		createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		
		//subresourceRange describes what the images purpose is and what should be accessed
		//images will be used as color targets without any mipmapping levels or multiple layers
		createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		createInfo.subresourceRange.baseMipLevel = 0;
		createInfo.subresourceRange.levelCount = 1;
		createInfo.subresourceRange.baseArrayLayer = 0;
		createInfo.subresourceRange.layerCount = 1;
		
		if ( vkCreateImageView( logicalDevice, &createInfo, nullptr, &swapChainImageViews.at(i) ) != VK_SUCCESS ) {
			throw std::runtime_error( "failed to create image views!" );
		}
	}

}



/*======================================================================
createGraphicsPipeline
	creates the graphics pipeline
========================================================================*/
void TriangleApplication::createGraphicsPipeline() {

	//now load the bytecode of the two (already compiled) shaders
	std::vector<char> vertShader = shader.readShaderFile( "../shaders/vert.spv" );
	std::vector<char> fragShader = shader.readShaderFile( "../shaders/frag.spv" );

	//handle to logicalDevice is needed for creation of a ShaderModule
	shader = Shader( logicalDevice );

	//only needed during the pipeline creation process
	//one ShaderModule for every available shader
	VkShaderModule vertShaderModule = shader.createShaderModule( vertShader );
	VkShaderModule fragShaderModule = shader.createShaderModule( fragShader );


	//now onto shader stage creation
	VkPipelineShaderStageCreateInfo vertShaderStageInfo = {};
	vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
	
	//specifiy shader module which contains the code and the entry point of the shader
	vertShaderStageInfo.module = vertShaderModule;
	vertShaderStageInfo.pName = "main";
	vertShaderStageInfo.pSpecializationInfo = nullptr;

	//do the same with the fragment shader module
	VkPipelineShaderStageCreateInfo fragShaderStageInfo = {};
	fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;

	fragShaderStageInfo.module = fragShaderModule;
	fragShaderStageInfo.pName = "main";
	fragShaderStageInfo.pSpecializationInfo = nullptr;

	//finish by defining an array that contains these two structs 
	//(we reference them in the actual pipeline creation step)
	VkPipelineShaderStageCreateInfo shaderStages[] = { vertShaderStageInfo, fragShaderStageInfo };
	

	//aquire the vertex data
	VkVertexInputBindingDescription bindingDescription = Vertex::getBindingDescription();
	std::array<VkVertexInputAttributeDescription, 3> attributeDescription = Vertex::getAttributeDescription();

	//VkPipelineVertexInputStateCreateInfo struct describes the format of the vertex data
	//that will be passed to the vertex shader
	//at the moment vertex attributes are in the shader itself so this
	//will be left out for the sake of simplicity
	VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
	vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertexInputInfo.vertexBindingDescriptionCount = 1;						//spacing between data and whether the data is per-vertex or per-instance
	vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;		// aquired binding description from vertex

	vertexInputInfo.vertexAttributeDescriptionCount = static_cast< uint32_t >(attributeDescription.size());					//type of attributes passed to the vertex shader, which binding to load them from and at which offset
	vertexInputInfo.pVertexAttributeDescriptions = attributeDescription.data();					//aquired attribute description from vertex

	//input assembly
	//VkPipelineInputAssemblyStateCreateInfo struct describes two things: what kind of geometry will
	//be drawn from the vertices and if primitive restart should be enabled
	VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
	inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;	//type of geometry
	inputAssembly.primitiveRestartEnable = VK_TRUE;


	//viewport creation
	VkViewport viewport = {};
	viewport.x = 0.0f;			//specify upper left corner
	viewport.y = 0.0f;			//specify upper left corner
	viewport.width = static_cast<float>(swapChainExtent.width);
	viewport.height = static_cast< float >(swapChainExtent.height);
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	//while viewports define the transformation from the image to
	//the framebuffer, scissor rectangles define in which regions pixel
	//will actually be stored. any pixels outside the scrissor rectangles
	//will be discarded by the rasterizer
	//so we specifiy an scissor rectnagle that covers it entirely
	VkRect2D scissor = {};
	scissor.offset = { 0, 0 };
	scissor.extent = swapChainExtent;		//from extent member

	//now this viewport and scissor rectangle need to be combined
	//into a viewport state using the VkPipelineStateCreateInfo struct
	VkPipelineViewportStateCreateInfo viewportState = {};
	viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportState.viewportCount = 1;
	viewportState.scissorCount = 1;
	viewportState.pViewports = &viewport;
	viewportState.pScissors = &scissor;

	//rasterizer stage
	//the rasterizer takes the geometry that is shaped by the vertices
	//from the vertex shader and turns it into fragments to be colored
	//by the fragment shader. it also performs depth testing, face culling
	//and the scissor test
	VkPipelineRasterizationStateCreateInfo rasterizer = {};
	rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizer.depthClampEnable = VK_FALSE;
	rasterizer.rasterizerDiscardEnable = VK_FALSE;		//if true then geometry never passes through the rasterizer stage. this basicly disables any output to the framebuffer

	rasterizer.polygonMode = VK_POLYGON_MODE_FILL;		//normal fill operation
	rasterizer.lineWidth = 1.0f;

	//enable culling mode
	rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
	rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;

	//no depth bias (useful for shadow mapping)
	//but disabled for now
	rasterizer.depthBiasEnable = VK_FALSE;
	rasterizer.depthBiasConstantFactor = 0.0f;
	rasterizer.depthBiasClamp = 0.0f;
	rasterizer.depthBiasSlopeFactor = 0.0f;

	//we're not using MSAA for now
	VkPipelineMultisampleStateCreateInfo multisampling = {};
	multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampling.sampleShadingEnable = VK_FALSE;
	multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	multisampling.minSampleShading = 1.0f;					//optional
	multisampling.pSampleMask = nullptr;					//optional
	multisampling.alphaToCoverageEnable = VK_FALSE;			//optional
	multisampling.alphaToOneEnable = VK_FALSE;				//optional
	
	//depth and stencil buffer creation (not used)
	VkPipelineDepthStencilStateCreateInfo depthStencilTesting = {};
	depthStencilTesting.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	depthStencilTesting.depthTestEnable = VK_TRUE;
	depthStencilTesting.depthWriteEnable = VK_TRUE;
	depthStencilTesting.depthCompareOp = VK_COMPARE_OP_LESS;
	depthStencilTesting.depthBoundsTestEnable = VK_FALSE;
	depthStencilTesting.minDepthBounds = 0.0f;
	depthStencilTesting.maxDepthBounds = 1.0f;
	depthStencilTesting.pNext = nullptr;

	//color blending
	//after a fragment shader has returned a color, it needs to be combined
	//with the color that is already in the framebuffer
	//this is known as color blending. there are two way to do it:
	//1. mixing the old and new value (we use this one)
	//2. combine old and new value using a bitwise operation 
	
	//configuration per attached framebuffer
	VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
	colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	colorBlendAttachment.blendEnable = VK_FALSE;
	colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;		
	colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;	
	colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;				
	colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;		
	colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;	
	colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;				

	//global color blending settings (uncomment to see what happens)
	VkPipelineColorBlendStateCreateInfo colorBlending = {};
	colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlending.logicOpEnable = VK_FALSE;
	colorBlending.logicOp = VK_LOGIC_OP_COPY; // Optional
	colorBlending.attachmentCount = 1;
	colorBlending.pAttachments = &colorBlendAttachment;
	colorBlending.blendConstants[0] = 0.0f; // Optional
	colorBlending.blendConstants[1] = 0.0f; // Optional
	colorBlending.blendConstants[2] = 0.0f; // Optional
	colorBlending.blendConstants[3] = 0.0f; // Optional

	//dynamic state
	//a limited amount of the state we've specified can catually be changed without recreating the pipeline
	//but if we want, we have to fill in a VkPipelineDynamicStateCreateInfo just like this
	//but right now we don't have dynamic states
	/*VkDynamicState dynamicStates[] = {
		VK_DYNAMIC_STATE_VIEWPORT,
		VK_DYNAMIC_STATE_LINE_WIDTH
	};

	VkPipelineDynamicStateCreateInfo dynamicState = {};
	dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	dynamicState.dynamicStateCount = 2;
	dynamicState.pDynamicStates = dynamicStates;*/


	//pipeline layout
	VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
	pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutInfo.setLayoutCount = 1;
	pipelineLayoutInfo.pSetLayouts = &descriptorSetLayout;
	pipelineLayoutInfo.pushConstantRangeCount = 0; //optional
	pipelineLayoutInfo.pPushConstantRanges = nullptr; //optional

	if ( vkCreatePipelineLayout( logicalDevice, &pipelineLayoutInfo, nullptr, &pipelineLayout ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to create pipeline layout" );
	}

	//create the graphics pipeline
	VkGraphicsPipelineCreateInfo pipelineInfo = {};
	pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipelineInfo.stageCount = 2;
	pipelineInfo.pStages = shaderStages;

	//now referencing all the structures
	pipelineInfo.pVertexInputState = &vertexInputInfo;
	pipelineInfo.pInputAssemblyState = &inputAssembly;
	pipelineInfo.pViewportState = &viewportState;
	pipelineInfo.pRasterizationState = &rasterizer;
	pipelineInfo.pMultisampleState = &multisampling;
	pipelineInfo.pDepthStencilState = &depthStencilTesting;			//optional
	pipelineInfo.pColorBlendState = &colorBlending;
	pipelineInfo.pDynamicState = nullptr;				//optional

	//reference all the structures describing the fixed-function stage
	pipelineInfo.layout = pipelineLayout;

	//referencing the render pass and the index of the sub pass where this graphics pipeline will be used
	pipelineInfo.renderPass = renderPass;
	pipelineInfo.subpass = 0;

	//we only have a single pipeline and now derivation of the graphics pipeline
	pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;	//optional
	pipelineInfo.basePipelineIndex = -1;				//optional

	if ( vkCreateGraphicsPipelines( logicalDevice, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &graphicsPipeline ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to create graphics pipeline" );
	}

	//cleanup after creation process
	vkDestroyShaderModule( logicalDevice, fragShaderModule, nullptr );
	vkDestroyShaderModule( logicalDevice, vertShaderModule, nullptr );

}



void TriangleApplication::createTextureImage( ) {

	int texWidth, texHeight, texChannels;

	stbi_uc* pixels = stbi_load( "../textures/texture.jpg", &texWidth, &texHeight, &texChannels, STBI_rgb_alpha );

	VkDeviceSize imageSize = texWidth * texHeight * 4;

	if ( !pixels ) {
		throw std::runtime_error( "failed to load texture image" );
	}

	VkImageCreateInfo stagingImageInfo = {};
	stagingImageInfo.sType =  VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	stagingImageInfo.imageType = VK_IMAGE_TYPE_2D;
	stagingImageInfo.extent.width = texWidth;
	stagingImageInfo.extent.height = texHeight;
	stagingImageInfo.extent.depth = 1;
	stagingImageInfo.mipLevels = 1;
	stagingImageInfo.arrayLayers = 1;
	stagingImageInfo.format = VK_FORMAT_R8G8B8A8_UNORM;
	stagingImageInfo.tiling = VK_IMAGE_TILING_LINEAR;                       //for some reason the tiling mode cannot be *_OPTIMAL
	stagingImageInfo.initialLayout = VK_IMAGE_LAYOUT_PREINITIALIZED;
	stagingImageInfo.usage = VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
	stagingImageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	stagingImageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
	stagingImageInfo.flags = 0;

	VmaAllocationCreateInfo stagingImageAllocCreateInfo = {};
	stagingImageAllocCreateInfo.usage = VMA_MEMORY_USAGE_CPU_ONLY;
	stagingImageAllocCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT;

	VkImage stagingImage = VK_NULL_HANDLE;
	VmaAllocation stagingImageAlloc = VK_NULL_HANDLE;
	VmaAllocationInfo stagingImageAllocInfo = {};

	//create buffer
	if ( vmaCreateImage( allocator, &stagingImageInfo, &stagingImageAllocCreateInfo, &stagingImage, &stagingImageAlloc, &stagingImageAllocInfo ) != VK_SUCCESS ) {
		throw std::runtime_error( "could not create image texture" );
	}


	memcpy( stagingImageAllocInfo.pMappedData, pixels, imageSize );
	//now the loaded image should be created and mapped to the staging buffer properly

	//free the stbi store since there is no need for it anymore
	stbi_image_free( pixels );

	//copy the texture to the gpu device memory
	VkImageCreateInfo imageInfo = {};
	imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageInfo.imageType = VK_IMAGE_TYPE_2D;
	imageInfo.extent.width = texWidth;
	imageInfo.extent.height = texHeight;
	imageInfo.extent.depth = 1;
	imageInfo.mipLevels = 1;
	imageInfo.arrayLayers = 1;
	imageInfo.format = VK_FORMAT_R8G8B8A8_UNORM;
	imageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
	imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	imageInfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
	imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
	imageInfo.flags = 0;

	VmaAllocationCreateInfo imageAllocCreateInfo = {};
	imageAllocCreateInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

	if ( vmaCreateImage( allocator, &imageInfo, &imageAllocCreateInfo, &textureImage, &texAlloc, nullptr ) != VK_SUCCESS ) {
		throw std::runtime_error( "could not create gpu image" );
	}

	VkCommandBufferAllocateInfo commandBufferAllocInfo = {};
	commandBufferAllocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	commandBufferAllocInfo.commandPool = commandPool;
	commandBufferAllocInfo.commandBufferCount = 1;
	commandBufferAllocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;

	VkCommandBuffer tempCmdBuffer;

	if ( vkAllocateCommandBuffers( logicalDevice, &commandBufferAllocInfo, &tempCmdBuffer ) != VK_SUCCESS ) {
		throw std::runtime_error( "could not allocate temporary command buffer" );
	}

	VkCommandBufferBeginInfo commandBufferBeginInfo = {};
	commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	
	if ( vkBeginCommandBuffer( tempCmdBuffer, &commandBufferBeginInfo ) != VK_SUCCESS ) {
		throw std::runtime_error( "could not begin command buffer recording" );
	}

	/***************************
	memory barriers for staging image and actual gpu image
	layout transitions
	*************************/
	
	//do layout transition for staging image
	VkImageMemoryBarrier memoryBarrier = {};
	memoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	memoryBarrier.oldLayout = VK_IMAGE_LAYOUT_PREINITIALIZED;
	memoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
	memoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	memoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	memoryBarrier.image = stagingImage;
	memoryBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	memoryBarrier.subresourceRange.baseMipLevel = 0;
	memoryBarrier.subresourceRange.levelCount = 1;
	memoryBarrier.subresourceRange.baseArrayLayer = 0;
	memoryBarrier.subresourceRange.layerCount = 1;
	memoryBarrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT;
	memoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

	vkCmdPipelineBarrier( tempCmdBuffer, VK_PIPELINE_STAGE_HOST_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &memoryBarrier );

	//do layout transition for gpu image
	memoryBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	memoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
	memoryBarrier.image = textureImage;
	memoryBarrier.srcAccessMask = 0;
	memoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

	vkCmdPipelineBarrier( tempCmdBuffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &memoryBarrier );

	//now we must copy the staging image to gpu image buffer
	VkImageCopy imageCopy = {};
	imageCopy.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	imageCopy.srcSubresource.baseArrayLayer = 0;
	imageCopy.srcSubresource.mipLevel = 0;
	imageCopy.srcSubresource.layerCount = 1;
	imageCopy.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	imageCopy.dstSubresource.baseArrayLayer = 0;
	imageCopy.dstSubresource.mipLevel = 0;
	imageCopy.dstSubresource.layerCount = 1;
	imageCopy.srcOffset.x = 0;
	imageCopy.srcOffset.y = 0;
	imageCopy.srcOffset.z = 0;
	imageCopy.dstOffset.x = 0;
	imageCopy.dstOffset.y = 0;
	imageCopy.dstOffset.z = 0;
	imageCopy.extent.width  = texWidth;
	imageCopy.extent.height = texHeight;
	imageCopy.extent.depth = 1;

	//now copy the staging image to texture image using command buffers
	vkCmdCopyImage( tempCmdBuffer, stagingImage, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, textureImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &imageCopy );

	//finally, we need to transition from DST_OPTIMAL to SHADER_READ_ONLY_OPTIMAL via memory barriers
	memoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
	memoryBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	memoryBarrier.image = textureImage;
	memoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
	memoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

	vkCmdPipelineBarrier( tempCmdBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0, nullptr, 0, nullptr, 1, &memoryBarrier );

	vkEndCommandBuffer( tempCmdBuffer );

	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &tempCmdBuffer;

	if ( vkQueueSubmit( graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE ) != VK_SUCCESS ) {
		throw std::runtime_error( "could not submit to graphics queue" );
	}

	vkQueueWaitIdle( graphicsQueue );

	if ( stagingImage != VK_NULL_HANDLE ) {
		vmaDestroyImage( allocator, stagingImage, stagingImageAlloc );
		stagingImage = VK_NULL_HANDLE;
	}

}



void TriangleApplication::createDescriptorLayout( ) {

	VkDescriptorSetLayoutBinding uboLayoutBinding = {};
	uboLayoutBinding.binding = 0;
	uboLayoutBinding.descriptorCount = 1;
	uboLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	uboLayoutBinding.pImmutableSamplers = nullptr;
	uboLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

	VkDescriptorSetLayoutBinding samplerLayoutBinding = {};
	samplerLayoutBinding.binding = 1;
	samplerLayoutBinding.descriptorCount = 1;
	samplerLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	samplerLayoutBinding.pImmutableSamplers = nullptr;
	samplerLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

	std::array<VkDescriptorSetLayoutBinding, 2> bindings = { uboLayoutBinding, samplerLayoutBinding };

	//create the first SET (consists of ubo and sampler)
	VkDescriptorSetLayoutCreateInfo layoutInfo = {};
	layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	layoutInfo.bindingCount = static_cast<uint32_t>(bindings.size( ));
	layoutInfo.pBindings = bindings.data( );


	if ( vkCreateDescriptorSetLayout( logicalDevice, &layoutInfo, nullptr, &descriptorSetLayout ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to create descriptor set layout!" );
	}

}



void TriangleApplication::createUniformBuffers( ) {

	VkDeviceSize bufferSize = sizeof( MVP );

	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = bufferSize;
	bufferInfo.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	VmaAllocationCreateInfo uniformAllocCreateInfo = {};
	uniformAllocCreateInfo.usage = VMA_MEMORY_USAGE_CPU_ONLY;
	uniformAllocCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT;
	
	uniformBuffer.resize( swapChainImages.size( ) );
	uniformAlloc.resize( swapChainImages.size( ) );
	uniformAllocInfo.resize( swapChainImages.size( ) );

	for ( int i = 0; i != swapChainImages.size( ); i++ ) {
		if ( vmaCreateBuffer( allocator, &bufferInfo, &uniformAllocCreateInfo, &uniformBuffer[i], &uniformAlloc[i], &uniformAllocInfo[i] ) != VK_SUCCESS ) {
			throw std::runtime_error( "failed to create uniform buffer" );
		}

		memcpy( uniformAllocInfo[i].pMappedData, &mvp, sizeof( mvp ) );
	}

}



void TriangleApplication::createTextureImageView( ) {

	//now we can create a ImageView
	VkImageViewCreateInfo textureImageViewInfo = {};
	textureImageViewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	textureImageViewInfo.image = textureImage;
	textureImageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	textureImageViewInfo.format = VK_FORMAT_R8G8B8A8_UNORM;
	textureImageViewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	textureImageViewInfo.subresourceRange.baseMipLevel = 0;
	textureImageViewInfo.subresourceRange.levelCount = 1;
	textureImageViewInfo.subresourceRange.baseArrayLayer = 0;
	textureImageViewInfo.subresourceRange.layerCount = 1;

	if ( vkCreateImageView( logicalDevice, &textureImageViewInfo, nullptr, &textureImageView ) != VK_SUCCESS ) {
		throw std::runtime_error( "could not create texture image view" );
	}

}




/*======================================================================
createFramebuffers
	creates the framebuffer vector
========================================================================*/
void TriangleApplication::createFramebuffers() {

	swapChainFramebuffers.resize( swapChainImageViews.size() );

	//fill the attachment array step by step
	for ( size_t i = 0; i != swapChainImageViews.size(); i++ ) {
		std::array<VkImageView, 2> attachments = { swapChainImageViews[i], depthImageView };

		VkFramebufferCreateInfo framebufferInfo = {};
		framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		framebufferInfo.renderPass = renderPass;
		framebufferInfo.attachmentCount = attachments.size( );
		framebufferInfo.pAttachments = attachments.data( );
		framebufferInfo.width = swapChainExtent.width;
		framebufferInfo.height = swapChainExtent.height;
		framebufferInfo.layers = 1;

		if ( vkCreateFramebuffer( logicalDevice, &framebufferInfo, nullptr, &swapChainFramebuffers[i] ) != VK_SUCCESS ) {
			throw std::runtime_error( "failed to create framebuffer" );
		}
	}

}



/*======================================================================
createCommandPool
	creates the command pool
	commands in vulkan, like drawing operations and memory transfers are 
	NOT executed directly using function calls. You have to record all
	the operations you want to perform in command buffer objects
	so the drawing commands can be done in advance and in multiple threads
========================================================================*/
void TriangleApplication::createCommandPool() {

	QueueFamilyIndices queueFamilyIndices = findQueueFamilies( physicalDevice );

	VkCommandPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily;
	poolInfo.flags = 0;			//optional

	/*
	NOTE: command buffers are executed by submitting them on one of the device
	queues, like the graphics and presentation queues we retrieved.
	each command pool can only allocate command buffers that are submitted
	on a single type of queue. we're going to record commands for drawing
	which is why we've chosen the graphics queue family
	*/

	if ( vkCreateCommandPool( logicalDevice, &poolInfo, nullptr, &commandPool ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to create command pool" );
	}

}



void TriangleApplication::createVertexBuffer() {

	VkDeviceSize bufferSize = sizeof( cube[0] ) * cube.size( );

	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = bufferSize;
	//buffer can be used as the src of a transfer command
	bufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	VmaAllocationCreateInfo vbAllocCreateInfo = {};
	vbAllocCreateInfo.usage = VMA_MEMORY_USAGE_CPU_ONLY;
	vbAllocCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT;

	VkBuffer stagingBuffer = VK_NULL_HANDLE;
	VmaAllocation stagingVertexBufferAlloc = VK_NULL_HANDLE;
	VmaAllocationInfo stagingVertexAllocInfo = {};

	if ( vmaCreateBuffer( allocator, &bufferInfo, &vbAllocCreateInfo, &stagingBuffer, &stagingVertexBufferAlloc, &stagingVertexAllocInfo  ) != VK_SUCCESS ) {
		throw std::runtime_error( "could not allocate vertex buffer" );
	}

	memcpy( stagingVertexAllocInfo.pMappedData, cube.data(), bufferSize );

	bufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
	vbAllocCreateInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
	vbAllocCreateInfo.flags = 0;

	if ( vmaCreateBuffer( allocator, &bufferInfo, &vbAllocCreateInfo, &vertexBuffer, &vertexAlloc, nullptr ) != VK_SUCCESS ) {
		throw std::runtime_error( "could not allocate vertex buffer" );
	}

	VkCommandBufferAllocateInfo commandBufferAllocInfo = {};
	commandBufferAllocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	commandBufferAllocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	commandBufferAllocInfo.commandBufferCount = 1;
	commandBufferAllocInfo.commandPool = commandPool;

	VkCommandBuffer tempCmdBuffer;
	vkAllocateCommandBuffers( logicalDevice, &commandBufferAllocInfo, &tempCmdBuffer );

	//now copy the buffer
	VkCommandBufferBeginInfo commandBufferBeginInfo = {};
	commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	vkBeginCommandBuffer( tempCmdBuffer, &commandBufferBeginInfo );

	VkBufferCopy copyRegion = {};
	copyRegion.srcOffset = 0;
	copyRegion.dstOffset = 0;
	copyRegion.size = bufferSize;

	vkCmdCopyBuffer( tempCmdBuffer, stagingBuffer, vertexBuffer, 1, &copyRegion );

	vkEndCommandBuffer( tempCmdBuffer );

	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &tempCmdBuffer;

	vkQueueSubmit( graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE );
	vkQueueWaitIdle( graphicsQueue );

	//now delete the temporary buffer
	if ( stagingBuffer != VK_NULL_HANDLE ) {
		vmaDestroyBuffer( allocator, stagingBuffer, stagingVertexBufferAlloc );
		stagingBuffer = VK_NULL_HANDLE;
	}

}



void TriangleApplication::createIndexBuffer() {
	VkDeviceSize indexBufferSize = sizeof( indices[0] ) * indices.size();

	VkBufferCreateInfo indexBufferInfo = {};
	indexBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	indexBufferInfo.size = indexBufferSize;
	//buffer can be used as the src of a transfer command
	indexBufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
	indexBufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	VmaAllocationCreateInfo inAllocCreateInfo = {};
	inAllocCreateInfo.usage = VMA_MEMORY_USAGE_CPU_ONLY;
	inAllocCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT;

	VkBuffer stagingIndexBuffer = VK_NULL_HANDLE;
	VmaAllocation stagingIndexBufferAlloc = VK_NULL_HANDLE;
	VmaAllocationInfo stagingIndexAllocInfo = {};

	if ( vmaCreateBuffer( allocator, &indexBufferInfo, &inAllocCreateInfo, &stagingIndexBuffer, &stagingIndexBufferAlloc, &stagingIndexAllocInfo ) != VK_SUCCESS ) {
		throw std::runtime_error( "could not allocate index buffer" );
	}

	memcpy( stagingIndexAllocInfo.pMappedData, indices.data( ), indexBufferSize );

	indexBufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
	inAllocCreateInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
	inAllocCreateInfo.flags = 0;

	if ( vmaCreateBuffer( allocator, &indexBufferInfo, &inAllocCreateInfo, &indexBuffer, &indexAlloc, nullptr ) != VK_SUCCESS ) {
		throw std::runtime_error( "could not allocate index buffer" );
	}

	VkCommandBufferAllocateInfo commandBufferAllocInfo = {};
	commandBufferAllocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	commandBufferAllocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	commandBufferAllocInfo.commandBufferCount = 1;
	commandBufferAllocInfo.commandPool = commandPool;

	VkCommandBuffer tempCmdBuffer;
	vkAllocateCommandBuffers( logicalDevice, &commandBufferAllocInfo, &tempCmdBuffer );

	//now copy the buffer
	VkCommandBufferBeginInfo commandBufferBeginInfo = {};
	commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	vkBeginCommandBuffer( tempCmdBuffer, &commandBufferBeginInfo );

	VkBufferCopy copyRegion = {};
	copyRegion.srcOffset = 0;
	copyRegion.dstOffset = 0;
	copyRegion.size = indexBufferSize;

	vkCmdCopyBuffer( tempCmdBuffer, stagingIndexBuffer, indexBuffer, 1, &copyRegion );

	vkEndCommandBuffer( tempCmdBuffer );

	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &tempCmdBuffer;

	vkQueueSubmit( graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE );
	vkQueueWaitIdle( graphicsQueue );

	//now delete the temporary buffer
	if ( stagingIndexBuffer != VK_NULL_HANDLE ) {
		vmaDestroyBuffer( allocator, stagingIndexBuffer, stagingIndexBufferAlloc );
		stagingIndexBuffer = VK_NULL_HANDLE;
	}

}



void TriangleApplication::updateUniformBuffer( uint32_t currentImage ) {
	static auto startTime = std::chrono::high_resolution_clock::now( );

	auto currentTime = std::chrono::high_resolution_clock::now( );
	float time = std::chrono::duration<float, std::chrono::seconds::period>( currentTime - startTime ).count( );

	MVP mvp = {};

	mvp.model = glm::rotate( glm::mat4( 1.0f ), time * glm::radians( 90.0f ), glm::vec3( 0.3f, 1.0f, 0.6f ) );
	mvp.model = glm::scale( mvp.model, glm::vec3( 0.5f, 0.5f, 0.5f ) );

	mvp.view  = glm::lookAt( glm::vec3( 2.0f, 2.0f, 2.0f ), glm::vec3( 0.0f, 0.0f, 0.0f ), glm::vec3( 0.0f, 1.0f, 0.0f ) );
	mvp.projection = glm::perspective( glm::radians( 45.0f ), swapChainExtent.width / (float)swapChainExtent.height, 0.1f, 10.0f );

	mvp.projection[1][1] *= -1;

	memcpy( uniformAllocInfo[currentImage].pMappedData, &mvp, sizeof(mvp) );
}



void TriangleApplication::createDescriptorPool( ) {

	std::array<VkDescriptorPoolSize, 2> poolSize = {};
	poolSize[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSize[0].descriptorCount = static_cast<uint32_t>(swapChainImages.size( ));
	poolSize[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	poolSize[1].descriptorCount = static_cast<uint32_t>(swapChainImages.size( ));

	VkDescriptorPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.poolSizeCount = poolSize.size();
	poolInfo.pPoolSizes = poolSize.data();
	poolInfo.maxSets = static_cast<uint32_t>(swapChainImages.size( ));

	if ( vkCreateDescriptorPool( logicalDevice, &poolInfo, nullptr, &descriptorPool ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to create descriptor pool!" );
	}

}



void TriangleApplication::createDepthBuffer( ) {
	
	VkFormat depthFormat = findDepthFormat( );

	if ( depthFormat == VK_FORMAT_UNDEFINED ) {
		throw std::runtime_error( "no suitable depth format available" );
	}

	//first create the depth image
	VkImageCreateInfo depthImageInfo = {};
	depthImageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	depthImageInfo.imageType = VK_IMAGE_TYPE_2D;
	depthImageInfo.extent.width = swapChainExtent.width;
	depthImageInfo.extent.height = swapChainExtent.height;
	depthImageInfo.extent.depth = 1;
	depthImageInfo.mipLevels = 1;
	depthImageInfo.arrayLayers = 1;
	depthImageInfo.format = depthFormat;
	depthImageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
	depthImageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	depthImageInfo.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
	depthImageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
	depthImageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	depthImageInfo.flags = 0;

	VmaAllocationCreateInfo depthAllocCreateInfo = {};
	depthAllocCreateInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;


	if ( vmaCreateImage( allocator, &depthImageInfo, &depthAllocCreateInfo, &depthImage, &depthAlloc, nullptr ) != VK_SUCCESS ) {
		throw std::runtime_error( "could not create image depth image" );
	}

	//now create the depth image view
	VkImageViewCreateInfo depthImageViewInfo = {};
	depthImageViewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	depthImageViewInfo.image = depthImage;
	depthImageViewInfo.format = depthFormat;
	depthImageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	depthImageViewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
	depthImageViewInfo.subresourceRange.baseMipLevel = 0;
	depthImageViewInfo.subresourceRange.levelCount = 1;
	depthImageViewInfo.subresourceRange.baseArrayLayer = 0;
	depthImageViewInfo.subresourceRange.layerCount = 1;

	if ( vkCreateImageView( logicalDevice, &depthImageViewInfo, nullptr, &depthImageView ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to create depth image view" );
	}

}



void TriangleApplication::createCommandBuffers() {

	commandBuffers.resize( swapChainFramebuffers.size() );

	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = commandPool;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;			//can be submitted to a queue for execution, but cannot be called from other command buffers
	allocInfo.commandBufferCount = ( uint32_t ) commandBuffers.size();

	if ( vkAllocateCommandBuffers( logicalDevice, &allocInfo, commandBuffers.data() ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to allocate command buffers" );
	}

	//starting command buffer recording
	for ( size_t i = 0; i != commandBuffers.size(); i++ ) {

		VkCommandBufferBeginInfo beginInfo = {};
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;		//the command buffer can be resubmitted while it also an already pending execution
		beginInfo.pInheritanceInfo = nullptr;		//optional

		if ( vkBeginCommandBuffer( commandBuffers[i], &beginInfo ) != VK_SUCCESS ) {
			throw std::runtime_error( "failed to begin recording command buffer" );
		}

		//starting a render pass
		VkRenderPassBeginInfo renderPassInfo = {};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		renderPassInfo.renderPass = renderPass;
		renderPassInfo.framebuffer = swapChainFramebuffers[i];				//we created a framebuffer for each swap chain image

		renderPassInfo.renderArea.offset = { 0, 0 };
		renderPassInfo.renderArea.extent = swapChainExtent;

		std::array<VkClearValue, 2> clearValues = { };
		clearValues[0].color = { 0.0f, 0.0f, 0.0f, 1.0f };
		clearValues[1].depthStencil = { 1.0, 0 };
		renderPassInfo.clearValueCount = clearValues.size( );
		renderPassInfo.pClearValues = clearValues.data( );

		//begin render pass
		vkCmdBeginRenderPass( commandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE );
	
			//basic drawing commands
			vkCmdBindPipeline( commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline );

			VkBuffer vertexBuffers[] = { vertexBuffer };
			VkDeviceSize offsets[] = { 0 };

			vkCmdBindVertexBuffers( commandBuffers[i], 0, 1, vertexBuffers, offsets );
			vkCmdBindDescriptorSets( commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1, descriptorSets.data(), 0, nullptr );
			
			vkCmdBindIndexBuffer( commandBuffers[i], indexBuffer, 0, VK_INDEX_TYPE_UINT16 );

			vkCmdDrawIndexed( commandBuffers[i], static_cast<uint32_t>(indices.size()), 1, 0, 0, 0 );

		//end render pass
		vkCmdEndRenderPass( commandBuffers[i] );

		if ( vkEndCommandBuffer( commandBuffers[i] ) != VK_SUCCESS ) {
			throw std::runtime_error( "failed to record command buffer" );
		}

	}

}



void TriangleApplication::createDescriptorSets( ) {

	std::vector<VkDescriptorSetLayout> layouts( swapChainImages.size( ), descriptorSetLayout );
	VkDescriptorSetAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = descriptorPool;
	allocInfo.descriptorSetCount = static_cast<uint32_t>(swapChainImages.size( ));
	allocInfo.pSetLayouts = layouts.data( );

	descriptorSets.resize( swapChainImages.size( ) );

	if ( vkAllocateDescriptorSets( logicalDevice, &allocInfo, descriptorSets.data() ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to allocate descriptor sets!" );
	}

	for ( size_t i = 0; i < swapChainImages.size( ); i++ ) {
		VkDescriptorBufferInfo bufferInfo = {};
		bufferInfo.buffer = uniformBuffer.at(i);
		bufferInfo.offset = 0;
		bufferInfo.range = sizeof( MVP );

		VkDescriptorImageInfo imageInfo = {};
		imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		imageInfo.imageView = textureImageView;
		imageInfo.sampler = textureSampler;

		std::array<VkWriteDescriptorSet, 2> descriptorWrites = {};

		descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		descriptorWrites[0].dstSet = descriptorSets[i];
		descriptorWrites[0].dstBinding = 0;
		descriptorWrites[0].dstArrayElement = 0;
		descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		descriptorWrites[0].descriptorCount = 1;
		descriptorWrites[0].pBufferInfo = &bufferInfo;

		descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		descriptorWrites[1].dstSet = descriptorSets[i];
		descriptorWrites[1].dstBinding = 1;
		descriptorWrites[1].dstArrayElement = 0;
		descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		descriptorWrites[1].descriptorCount = 1;
		descriptorWrites[1].pImageInfo = &imageInfo;

		vkUpdateDescriptorSets( logicalDevice, descriptorWrites.size(), descriptorWrites.data(), 0, nullptr );
	}


}



void TriangleApplication::createTextureSampler( ) {
	VkSamplerCreateInfo samplerInfo = {};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.magFilter = VK_FILTER_LINEAR;
	samplerInfo.minFilter = VK_FILTER_LINEAR;
	samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.anisotropyEnable = VK_TRUE;
	samplerInfo.maxAnisotropy = 16.0f;
	samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
	samplerInfo.unnormalizedCoordinates = VK_FALSE;
	samplerInfo.compareEnable = VK_FALSE;
	samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
	samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	samplerInfo.mipLodBias = 0.0f;
	samplerInfo.minLod = 0.0f;
	samplerInfo.maxLod = 0.0f;

	if ( vkCreateSampler( logicalDevice, &samplerInfo, nullptr, &textureSampler ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to create texture sampler" );
	}

}



QueueFamilyIndices TriangleApplication::findQueueFamilies( VkPhysicalDevice device ) {
	
	QueueFamilyIndices indices;

	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties( device, &queueFamilyCount, nullptr );

	std::vector<VkQueueFamilyProperties> queueFamilies( queueFamilyCount );
	vkGetPhysicalDeviceQueueFamilyProperties( device, &queueFamilyCount, queueFamilies.data() ); 

	int i = 0;
	//looking for a queue that supports graphics commands
	for ( const VkQueueFamilyProperties& queueFamily : queueFamilies ) {
		if ( queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT ) {
			indices.graphicsFamily = i;
		}

		VkBool32 presentSupport = false;
		vkGetPhysicalDeviceSurfaceSupportKHR( device, i, surface, &presentSupport );

		if ( queueFamily.queueCount > 0 && presentSupport ) {
			indices.presentFamily = i;
		}

		//check for (very basic) completeness
		if ( indices.isComplete() ) {
			break;
		}
		i++;
	}

	return indices;
}



SwapChainSupportDetails TriangleApplication::querySwapChainSupport( VkPhysicalDevice device ) {
	
	SwapChainSupportDetails details;

	vkGetPhysicalDeviceSurfaceCapabilitiesKHR( device, surface, &details.capabilites);
	
	//querying the supported surface formats
	uint32_t formatCount;
	vkGetPhysicalDeviceSurfaceFormatsKHR( device, surface, &formatCount, nullptr );

	if ( formatCount != 0 ) {
		details.formats.resize( formatCount );
		vkGetPhysicalDeviceSurfaceFormatsKHR( device, surface, &formatCount, details.formats.data() );
	}


	//querying the supported presentation modes
	uint32_t presentModeCount;
	vkGetPhysicalDeviceSurfacePresentModesKHR( device, surface, &presentModeCount, nullptr );

	if ( presentModeCount != 0 ) {
		details.presentModes.resize( presentModeCount );
		vkGetPhysicalDeviceSurfacePresentModesKHR( device, surface, &presentModeCount, details.presentModes.data() );
	}

	return details;
}


/*
================================================================
chooseSwapSurfaceFormat
	find the right settings for the best possible swap chain
	determines the surface format
================================================================
*/
VkSurfaceFormatKHR TriangleApplication::chooseSwapSurfaceFormat( const std::vector<VkSurfaceFormatKHR>& availableFormats ) {


	//best case
	if ( availableFormats.size() && availableFormats.at( 0 ).format == VK_FORMAT_UNDEFINED ) {
		return { VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
	}

	//search linearly through the available formats for a preferred combination
	for ( const VkSurfaceFormatKHR& availFormat : availableFormats ) {
		if ( availFormat.format == VK_FORMAT_B8G8R8A8_UNORM  && availFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR ) {
			return availFormat;
		}
	}

	//just settle with the first specified surface format
	return availableFormats[0];
}



/*
================================================================
chooseSwapPresentMode
	chooses best swap present mode based on the
	device capabilities
================================================================
*/
VkPresentModeKHR TriangleApplication::chooseSwapPresentMode( const std::vector<VkPresentModeKHR>& availablePresentModes ) {
	
	VkPresentModeKHR bestMode = VK_PRESENT_MODE_FIFO_KHR;

	//search for triple buffering capabilites
	for ( const VkPresentModeKHR& presentMode : availablePresentModes ) {
		if ( presentMode == VK_PRESENT_MODE_MAILBOX_KHR ) {
			bestMode = presentMode;
		} else if ( presentMode == VK_PRESENT_MODE_IMMEDIATE_KHR ) {
			//since some drivers currently don't properly support VK_PRESENT_MODE_FIFO_KHR
			bestMode = presentMode;
		}
	}
	return bestMode;
}



/*
================================================================
chooseSwapExtent
	choose swap extent (resolution of images in swap chain)
================================================================
*/
VkExtent2D TriangleApplication::chooseSwapExtent( const VkSurfaceCapabilitiesKHR & capabilities ) {
	
	if ( capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max() ) {
		return capabilities.currentExtent;
	} else {
		int width, height;

		glfwGetFramebufferSize( window, &width, &height );

		VkExtent2D actualExtent = { static_cast<uint32_t>(width), static_cast<uint32_t>(height) };

		actualExtent.width = std::max( capabilities.minImageExtent.width, std::min( capabilities.maxImageExtent.width, actualExtent.width ) );
		actualExtent.height = std::max( capabilities.minImageExtent.height, std::min( capabilities.maxImageExtent.height, actualExtent.height ) );

		return actualExtent;
	}
}



/*
================================================================
getRequiredExtension
	relay debug messages back to the programm
	to receive those messages we have to set up a callback
	NOTE: requires VK_EXT_debug_report extension
================================================================
*/
std::vector<const char*> TriangleApplication::getRequiredExtensions() {
	uint32_t glfwExtensionCount = 0;
	const char** glfwExtensions;

	glfwExtensions = glfwGetRequiredInstanceExtensions( &glfwExtensionCount );

	std::vector<const char*> extensions( glfwExtensions, glfwExtensions + glfwExtensionCount );

	if ( enableValidationLayers ) {
		extensions.push_back( VK_EXT_DEBUG_REPORT_EXTENSION_NAME );
	}
	return extensions;
}



VKAPI_ATTR VkBool32 VKAPI_CALL TriangleApplication::debugCallback( VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType, uint64_t obj, size_t location, int32_t code, const char * layerPrefix, const char * msg, void * userData ) {
	
	std::cerr << "validation layer: " << msg << std::endl;

	return VK_FALSE;
}



/*
================================================================
createInstance
	initializes the vulkan library by creating an instance.
	the instance is the connection between the application 
	and the vulkan library.
================================================================
*/
void TriangleApplication::createInstance(){

	if ( enableValidationLayers && !checkValidationLayerSupport() ) {
		throw std::runtime_error( "validation layers requested, but not available" );
	}

	//get (global) extensions for window system interfacing
	//use GLFW function that returns the extension(s) it needs
	uint32_t glfwExtensionsCount = 0;
	const char** glfwExtensions;

	glfwExtensions = glfwGetRequiredInstanceExtensions( &glfwExtensionsCount );

	//checkInstanceExtensions( glfwExtensions, glfwExtensionsCount );			//disabled for now

	VkApplicationInfo appInfo = {};		//RAII
	//provide useful information for the driver to optimize the application
	//but otherwise enterily optional
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pApplicationName = "VKTriangle";
	appInfo.applicationVersion = VK_MAKE_VERSION( 1, 0, 0 );
	appInfo.pEngineName = "No Engine";
	appInfo.engineVersion = VK_MAKE_VERSION( 1, 0, 0 );
	appInfo.apiVersion = VK_API_VERSION_1_1;

	//not optional anymore
	//tells the vulkan driver which global extensions 
	//and validation layers we want to use
	VkInstanceCreateInfo createInfo = {};	//RAII

	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pApplicationInfo = &appInfo;

	//set required extension info to vulkan
	std::vector<const char*> extensions = getRequiredExtensions();
	createInfo.enabledExtensionCount = extensions.size();
	createInfo.ppEnabledExtensionNames = extensions.data();

	if ( enableValidationLayers ) {
		createInfo.enabledLayerCount = validationLayers.size();
		createInfo.ppEnabledLayerNames = validationLayers.data();
	} else {
		createInfo.enabledLayerCount = 0;
	}

	//now finally create an vulkan instance
	if ( vkCreateInstance( &createInfo, nullptr, &instance ) != VK_SUCCESS ) {
		throw std::runtime_error( "failed to create vkInstance" );
	} else {
		//std::cout << "vkInstance created succesfully" << std::endl;	//output is not needed for now
	}

}



VkFormat TriangleApplication::findDepthFormat( ) {

	std::vector<VkFormat> formats;
	formats.push_back( VK_FORMAT_D32_SFLOAT );
	formats.push_back( VK_FORMAT_D32_SFLOAT_S8_UINT );
	formats.push_back( VK_FORMAT_D24_UNORM_S8_UINT );

	return findSupportedFormat( formats, VK_IMAGE_TILING_OPTIMAL, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT );
}



VkFormat TriangleApplication::findSupportedFormat( const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features ) {

	for ( VkFormat format : candidates ) {
		VkFormatProperties props;

		vkGetPhysicalDeviceFormatProperties( physicalDevice, format, &props );

		if ( (tiling == VK_IMAGE_TILING_LINEAR) && ((props.linearTilingFeatures & features) == features) ) {
			return format;
		} else if ( (tiling == VK_IMAGE_TILING_OPTIMAL) && ((props.optimalTilingFeatures & features) == features) ) {
			return format;
		}
	}

	return VK_FORMAT_UNDEFINED;
}



void TriangleApplication::key_callback( GLFWwindow * window, int key, int scancode, int action, int mods ) {
	if ( key == GLFW_KEY_ESCAPE && action == GLFW_PRESS ) {
		glfwSetWindowShouldClose( window, GLFW_TRUE );
	}
}



void TriangleApplication::framebufferResizeCallback( GLFWwindow * window, int width, int height ) {
	TriangleApplication* app = reinterpret_cast< TriangleApplication* >(glfwGetWindowUserPointer( window ));
	app->framebufferResized = true;
}