#include "../include/TriangleApplication.h"

#include <iostream>


int main() {

	TriangleApplication triangleApp;

	try {
		triangleApp.run();
	} catch ( const std::runtime_error& e ) {
		std::cerr << e.what() << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}