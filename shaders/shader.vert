#version 450
//required for vulkan shaders
#extension GL_ARB_separate_shader_objects : enable	


layout (location = 0) in vec3 inPosition;
layout (location = 1) in vec3 inColor;
layout (location = 2) in vec2 inTexCoord;

layout (location = 0) out vec3 fragColor;
layout (location = 1) out vec2 fragTexCoord;

layout (set = 0, binding = 0) uniform MVP {
	mat4 model;
	mat4 view;
	mat4 projection;
} mvp;

void main(){
	//gl_VertexIndex contains the index of the current vertex

	gl_Position = mvp.projection * mvp.view * mvp.model * vec4(inPosition, 1.0);
	fragColor = inColor;
	fragTexCoord = inTexCoord;
}