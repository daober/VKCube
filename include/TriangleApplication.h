//#define GLFW_INCLUDE_VULKAN
#include <vulkan\vulkan.h>
#include <GLFW\glfw3.h>

#include <stdexcept>
#include <functional>
#include <cstdlib>
#include <vector>


#include "../include/Shader.h"
#include "../include/vk_mem_alloc.h"
#include "../include/stb_image.h"

#include "glm\glm.hpp"



//SCREEN_SIZE
static const int SCR_WIDTH = 800;
static const int SCR_HEIGHT = 600;

//declares multiple frame to be "in-flight" while still bounding the 
//amount of work that piles up
const int MAX_FRAMES_IN_FLIGHT = 2;

//enable validation layers
const std::vector<const char*> validationLayers = {
	"VK_LAYER_LUNARG_standard_validation"
};

const std::vector<const char*> deviceExtensions = {
	VK_KHR_SWAPCHAIN_EXTENSION_NAME
};


//enable validation layers in debug builds
#ifdef NDEBUG
const bool enableValidationLayers = false;
#else
const bool enableValidationLayers = true;
#endif


struct QueueFamilyIndices {
	int graphicsFamily = -1;		//0 denotes to 'not found'
	int presentFamily = -1;

	//basic check if there is at least one supported graphics family available 
	bool isComplete() {				
		if ( graphicsFamily >= 0 && presentFamily >= 0) {
			return true;
		} else {
			return false;
		}
	}
};


//for querying details of swap chain support
struct SwapChainSupportDetails {
	VkSurfaceCapabilitiesKHR capabilites;
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> presentModes;
};


struct MVP {
	glm::mat4 model;
	glm::mat4 view;
	glm::mat4 projection;
};



class TriangleApplication {

public:

	void run();

	bool framebufferResized = false;

private:
	void initVulkan();

	void initWindow();

	void createInstance();

	VkFormat findSupportedFormat( const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features );

	VkFormat findDepthFormat( );

	void loop();

	void cleanup();

	bool checkValidationLayerSupport();

	void setupDebugCallback();

	void pickPhysicalDevice();

	void createLogicalDevice();

	void createSwapChain();

	void createVMAllocator( );

	void createSurface();

	void createImageViews();

	void createGraphicsPipeline();

	void createTextureImage( );

	void createDescriptorLayout( );

	void createUniformBuffers( );

	void createTextureImageView( );

	void createFramebuffers();

	void createCommandPool();

	void createVertexBuffer();

	void createIndexBuffer();

	void updateUniformBuffer( uint32_t currentImage );

	void createDescriptorPool( );

	void createDepthBuffer( );

	void createCommandBuffers();

	void createDescriptorSets( );

	void createTextureSampler( );

	void createRenderPass();

	void cleanupSwapChain();

	void createSyncObjects();

	void recreateSwapChain();

	uint32_t findMemoryType( uint32_t typeFilter, VkMemoryPropertyFlags property );

	void drawFrame();

	bool isDeviceSuitable( VkPhysicalDevice device );

	bool checkDeviceExtensionSupport( VkPhysicalDevice device );

	void checkInstanceExtensions(const char** glfwExtensions = nullptr, const uint32_t glfwExtCount = 0 );

	QueueFamilyIndices findQueueFamilies( VkPhysicalDevice device );

	SwapChainSupportDetails querySwapChainSupport( VkPhysicalDevice device );

	VkSurfaceFormatKHR chooseSwapSurfaceFormat( const std::vector<VkSurfaceFormatKHR>& availableFormats );

	VkPresentModeKHR chooseSwapPresentMode( const std::vector<VkPresentModeKHR>& availablePresentModes );

	VkExtent2D chooseSwapExtent( const VkSurfaceCapabilitiesKHR& capabilities );

	std::vector<const char*> getRequiredExtensions();

	//validation layer callback for vulkan
	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback( 
		VkDebugReportFlagsEXT flags,
		VkDebugReportObjectTypeEXT objType,
		uint64_t obj,
		size_t location,
		int32_t code,
		const char* layerPrefix,
		const char* msg,
		void* userData 
	);

	//GLFW Callback
	static void key_callback( GLFWwindow* window, int key, int scancode, int action, int mods );
	static void framebufferResizeCallback( GLFWwindow* window, int width, int height );

private:

	GLFWwindow* window;			//glfw window instance

	VkInstance					instance;								//handle to a vulkan instance 
	VkDebugReportCallbackEXT	validationCallback;						//handle validation to a callback instance
	
	//surface creation
	VkSurfaceKHR				surface;								//handle to the surface
	
	VkPhysicalDevice			physicalDevice = VK_NULL_HANDLE;		//handle to physical device instance
	VkDevice					logicalDevice;							//handle to a logical device instance
	VkQueue						graphicsQueue;							//handle to the graphics queue
	VkQueue						presentQueue;							//handle to the present queue
	VkSwapchainKHR				swapChain;								//handle to the swap chain

	VkFormat					swapChainImageFormat;
	VkExtent2D					swapChainExtent;

	std::vector<VkImage>		swapChainImages;
	
	//to use the vkImage from the swap chain in the render pipeline we have to create VkImageView objects
	std::vector<VkImageView>	swapChainImageViews;

	Shader						shader;									//simple Shader class helper as member
	VkRenderPass				renderPass;
	VkPipelineLayout			pipelineLayout;							//handle to the pipeline layout
	VkPipeline					graphicsPipeline;						//handle to the graphics pipeline          

	std::vector<VkFramebuffer>	swapChainFramebuffers;

	VkCommandPool				commandPool;

	std::vector<VkCommandBuffer> commandBuffers;

	//each frame should have its own set of semaphores
	std::vector<VkSemaphore>	imageAvailableSemaphore;
	std::vector<VkSemaphore>	renderFinishedSemaphore;
	//fences are mainly designed to syncrhonize your application
	//itself with rendering operation, whereas semaphores are used to
	//synchronize operations within or across command queues
	std::vector<VkFence>		inFlightFences;

	VkBuffer					vertexBuffer;
	VkBuffer                    indexBuffer;

	VkImage                     textureImage;
	VkImage                     depthImage;
	VkSampler                   textureSampler;
	std::vector<VkBuffer>       uniformBuffer;

	VkImageView                 textureImageView;
	VkImageView                 depthImageView;

	VmaAllocator                allocator;

	VmaAllocation               texAlloc;
	VmaAllocation               vertexAlloc;
	VmaAllocation               indexAlloc;
	VmaAllocation               depthAlloc;
	std::vector<VmaAllocation>  uniformAlloc;
	
	VkDescriptorSetLayout       descriptorSetLayout;
	
	std::vector<VmaAllocationInfo>   uniformAllocInfo;

	MVP                          mvp;

	VkDescriptorPool             descriptorPool;
	std::vector<VkDescriptorSet> descriptorSets;

	size_t currentFrame = 0;
};