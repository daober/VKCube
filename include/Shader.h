#pragma once

#include <vector>
#include <string>


/*======================================================
Shader
	just a simple helper class
	excepts already compiled shader to SPIR-V bytecode
=======================================================*/
class Shader {

public:

	Shader() = default;

	Shader( const VkDevice& dev );

	void init(const VkDevice& dev );

	std::vector<char> readShaderFile( const std::string& filename );

	VkShaderModule createShaderModule( const std::vector<char>& byteCode );

private:

	VkDevice logicalDevice;

};